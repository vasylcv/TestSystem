﻿<?php

ini_set('display_errors', 1);

require_once 'config.php';
require_once 'app/core/Model.php';
require_once 'app/core/View.php';
require_once 'app/core/Controller.php';
require_once 'app/models/Validation.php';
require_once 'app/core/Connection.php';

require_once 'app/core/Route.php';
session_start();
Route::start();

?>


