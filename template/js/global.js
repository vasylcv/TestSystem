$('document').ready(function() {
       $('#myModal').modal("show");

            setTimeout(function(){$('#successdiv').fadeOut('fast')},3000);
            setTimeout(function(){$('#alertdiv').fadeOut('fast')},6000);

    var max_fields      = 50;
    var wrapper         = $(".input_fields_wrap");
    var add_button      = $(".add_field_button");

    var x = 1;
    $(add_button).click(function(e){
        e.preventDefault();
        if(x < max_fields){
            x++;
            $(wrapper).append('<div>' +
                    '<div class="form-group"><label class="col-md-2 control-label">Питання №'+ x +'</label>'+
                    '<div class="col-md-10"><input type="text" class="form-control" placeholder="Питання" name="question['+ x +'][]" required></div></div>'+
                    '<div class="form-group"><label class="col-md-2 control-label">Варіант відповіді №1</label>' +
                    '<div class="col-md-9"><input type="text" class="form-control" placeholder="Відповідь" name="variant['+ x +'][]" required></div>' +
                    '<div class="col-md-1"><label class="radio-inline"><input type="radio" name="correct['+ x +'][]" value="1" required></label></div></div>' +
                    '<div class="form-group"><label class="col-md-2 control-label">Варіант відповіді №2</label>' +
                    '<div class="col-md-9"><input type="text" class="form-control" placeholder="Відповідь" name="variant['+ x +'][]" required></div>' +
                    '<div class="col-md-1"><label class="radio-inline"><input type="radio" name="correct['+ x +'][]" value="2"></label></div></div>' +
                    '<div class="form-group"><label class="col-md-2 control-label">Варіант відповіді №3</label>' +
                    '<div class="col-md-9"><input type="text" class="form-control" placeholder="Відповідь" name="variant['+ x +'][]" required></div>' +
                    '<div class="col-md-1"><label class="radio-inline"><input type="radio" name="correct['+ x +'][]" value="3"></label></div></div>' +
                    '<div class="form-group"><label class="col-md-2 control-label">Варіант відповіді №4</label>' +
                    '<div class="col-md-9"><input type="text" class="form-control" placeholder="Відповідь" name="variant['+ x +'][]" required></div>' +
                    '<div class="col-md-1"><label class="radio-inline"><input type="radio" name="correct['+ x +'][]" value="4"></label></div></div>' +
                    '<div class="form-group"><label class="col-md-2 control-label">Оцінка</label>' +
                    '<div class="col-md-10"><input type="number" class="form-control" placeholder="Оцінка" name="mark['+ x +'][]" required></div></div>' +
                    '<a href="#" class="btn btn-danger btn-sm remove-field pull-right" >Видалити блок з запитанням №'+ x +' </a><br /><br />' +
                '</div>');

        }
    });

    $(wrapper).on("click",".remove-field", function(e){
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });


    $('.cont').addClass('hide');
    count=$('.questions').length;
    $('#question'+1).removeClass('hide');

    $(document).on('click','.next',function(){
        element=$(this).attr('id');
        last = parseInt(element.substr(element.length - 1));
        nex=last+1;
        $('#question'+last).addClass('hide');

        $('#question'+nex).removeClass('hide');
    });

    $(document).on('click','.previous',function(){
        element=$(this).attr('id');
        last = parseInt(element.substr(element.length - 1));
        pre=last-1;
        $('#question'+last).addClass('hide');

        $('#question'+pre).removeClass('hide');
    });
});

