<?php

class Route
{

	static function start()
	{
		$controller_name = 'Index';
		$action_name = 'execute';

		$routes = explode('/', $_SERVER['REQUEST_URI']);

		$rout_get = explode('?', $_SERVER['REQUEST_URI']);

		if (!empty($routes[1]) && empty($rout_get[1]))
		{
			$controller_name = ucfirst($routes[1]);
        }
        if (!empty($rout_get[1])){

            $controller_name = ucfirst(substr($rout_get[0],1));

        }

		if (!empty($routes[2]))
		{
			$action_name = $routes[2];

		}

		   $model_name = $controller_name;
		   $controller_name = $controller_name.'Controller';

		    $model_file = $model_name.'.php';
		    $model_path = "app/models/".$model_file;

				if(file_exists($model_path))
				{
					include "app/models/".$model_file;
				}

		$controller_file = $controller_name.'.php';
		$controller_path = "app/controllers/".$controller_file;

		if(file_exists($controller_path))
		{
			include "app/controllers/".$controller_file;
		}
		else
		{
			Route::ErrorPage();
		}
		
			$controller = new $controller_name;
			$action = $action_name;

				if(method_exists($controller, $action))
				{
					$controller->$action();
				}
				else
				{
					Route::ErrorPage();
				}
	
	}

	static function ErrorPage()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'/Error');
    }
    
}
