<?php

class Controller {

	function __construct()
	{
		$this->view = new View();
	}

	function execute()
	{

	}

	static function redirect($path)
	{
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/'.$path;
		return header('Location:'.$host);
	}

}
