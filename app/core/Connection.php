<?php

$dbObject = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
$dbObject->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbObject->exec('SET NAMES utf8');

?>