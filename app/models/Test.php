<?php
require_once 'app/models/Question.php';
require_once 'app/models/QuestionTest.php';
class Test extends Model
{
    private $title;
    private $description;

    public function set_test($title, $description, $arr_res)
    {
        if (!empty($title) && !empty($description) && !empty($arr_res))
        {
            $question = new Question();
            $questionTest = new QuestionTest();

            $title = Validation::validStr($title);
            $description = Validation::validStr($description);

            try {
                $this->db->beginTransaction();

                $sql = "INSERT INTO test (title,description) VALUES (
                :title,
                :description)";

                $addTest = $this->db->prepare($sql);

                $addTest->bindParam(':title', $title, PDO::PARAM_STR);
                $addTest->bindParam(':description', $description, PDO::PARAM_STR);

                $addTest->execute();

                $id_test = $this->db->lastInsertId();

                $arr_id = array();
                foreach($arr_res as $k=>$val) {

                    foreach($val['question'] as $q_item){
                        $questions = Validation::validStr($q_item);
                    }

                    foreach($val['correct'] as $c_item){
                        $correct = Validation::validStr($c_item);

                    }
                    foreach($val['mark'] as $m_item){
                        $mark = Validation::validStr($m_item);

                    }

                    $addQuestion = $question->set_questions($questions, $val['variant'][0], $val['variant'][1], $val['variant'][2], $val['variant'][3], $correct, $mark);

                    $id_question = $this->db->lastInsertId();

                    array_push($arr_id, $id_question);
                }

                    foreach ($arr_id as $ques_id) {
                        $addQuestionTest = $questionTest->set_dat($ques_id, $id_test);
                    }

                $this->db->commit();

                return true;
            }
            catch( PDOException $e ) {

                return $e->getMessage();

            }

        } else {
            return false;
        }
    }

    public function get_test(){
        try {
            $res = $this->db->query('SELECT t.id, t.title, t.description, COUNT(q.id) as "count" 
                                  FROM test t left join question_test q on t.id = q.test_id GROUP BY t.id');

            if (!$res) {
                return FALSE;
            }

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }


    public function get_mark($test_id){

        try {
            $res = $this->db->prepare('SELECT t.id, t.title, t.description, SUM(qus.mark) as "mark" FROM test t 
                        LEFT JOIN question_test q ON t.id = q.test_id
                        LEFT JOIN question qus ON q.question_id = qus.id
                        WHERE t.id = ?
                        GROUP BY t.id');

            if (!$res) {
                return FALSE;
            }

            $res->execute([$test_id]);

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }
}