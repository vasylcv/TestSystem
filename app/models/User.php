<?php

class User extends Model
{
    private $login;
    private $password;

    public function login($login, $pass){

        if (!empty($login) && !empty($pass)){

            try {

                $password = md5(md5($pass));
                $query = $this->db->prepare("SELECT id, login, role_id FROM users WHERE users.login = ? AND users.password = ?");

                if (!$query) {
                    return FALSE;
                }

                $query->execute([$login, $password]);
                $data = $query->fetchAll(PDO::FETCH_ASSOC);

                return $data;
            }
            catch(PDOException $e ) {

                return $e->getMessage();
            }
        } else {

            return false;
        }
    }
}