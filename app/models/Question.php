<?php

class Question extends Model
{
    private $title;
    private $type;
    private $mark;

    public function set_questions($question, $variant1, $variant2, $variant3, $variant4, $correct, $mark)
    {
        if (!empty($question))
        {


            try {
                $sql = "INSERT INTO question (title, variant1, variant2, variant3, variant4, correct, mark) VALUES (
                :title,
                :variant1,
                :variant2,
                :variant3,
                :variant4,
                :correct,
                :mark)";

                $addQuestion = $this->db->prepare($sql);

                $addQuestion->bindParam(':title', $question, PDO::PARAM_STR);
                $addQuestion->bindParam(':variant1', $variant1, PDO::PARAM_STR);
                $addQuestion->bindParam(':variant2', $variant2, PDO::PARAM_STR);
                $addQuestion->bindParam(':variant3', $variant3, PDO::PARAM_STR);
                $addQuestion->bindParam(':variant4', $variant4, PDO::PARAM_STR);
                $addQuestion->bindParam(':correct', $correct, PDO::PARAM_STR);
                $addQuestion->bindParam(':mark', $mark, PDO::PARAM_STR);

                $addQuestion->execute();

                return true;
            }
            catch( PDOException $e ) {

                return $e->getMessage();

            }

        } else {
            return false;
        }
    }

    public function get_data($id)
    {
        try {
            $res = $this->db->prepare('SELECT * FROM question WHERE id = ?');

            if (!$res) {
                return FALSE;
            }

            $res->execute([$id]);

            $data = $res->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }

    public function check_answer($user_id, $test_id){

        try {
            $sql = ('SELECT SUM(q.mark) as \'bal\' FROM question_user qu
                      LEFT JOIN question q ON qu.question_id = q.id
                      WHERE qu.user_id = :user_id
                      AND qu.question_id IN (SELECT qt.question_id FROM question_test as qt WHERE qt.test_id = :test_id) 
                      AND qu.answer = (SELECT correct FROM question WHERE id = qu.question_id)');

            $check = $this->db->prepare($sql);

            $check->bindParam(':user_id', $user_id, PDO::PARAM_STR);
            $check->bindParam(':test_id', $test_id, PDO::PARAM_STR);

            $check->execute();

            if (!$check) {
                return FALSE;
            }

            $data = $check->fetchAll(PDO::FETCH_ASSOC);

            return $data;
        }
        catch( PDOException $e ) {

            return $e->getMessage();
        }
    }
}