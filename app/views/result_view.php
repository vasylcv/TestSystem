<div class="container">
    <div class="row archive">
           <h4><a href="archive" class="btn bg-primary">Архів</a></h4>
    </div>
    <div class="row text-center">
        <h1>Рузультати тесту</h1>
        <h2>Ви набрали: <strong><?php echo $data['bal'];?></strong> з можливих <?php echo $data['mark'];?>.</h2>
        <h4>Успішність <?php echo $data['percent'];?> %.</h4>
    </div>
    <div class="row">
        <div class="table table-condensed">
            <table class="table">
                <tr><th>Запитання</th><th>Ваша відповідь</th><th>Правильна відповідь</th></tr>
            <?php foreach($data_table as $k=>$item):?>
                <?php if($item['user_answer'] == $item['correct_answer']):?>
                <tr class="success"><td><?php echo $item['question'];?></td>
                    <td><?php echo htmlentities($item['user_answer']);?></td>
                    <td><?php echo htmlentities($item['correct_answer']);?></td>
                </tr>
                <?php elseif ($item['user_answer'] != $item['correct_answer'] || $item['user_answer'] == NULL):?>
                    <tr class="danger"><td><?php echo $item['question'];?></td>
                        <td><?php echo htmlentities($item['user_answer']);?></td>
                        <td><?php echo htmlentities($item['correct_answer']);?></td>
                    </tr>
                <?php endif;?>
             <?php endforeach;?>
            </table>
        </div>
    </div>
</div>



