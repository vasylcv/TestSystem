<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Для того щоб увійти на сайт введіть логін і пароль</h4>
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal" method="POST">
                    <div class="form-group">
                        <label for="inputLogin" class="col-sm-2 control-label">Login</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Вхід</button>
                        </div>
                    </div>
                </form>
                <p>Для адмін частини - логін/пароль: admin/admin</p>
                <p>Для проходження тесту - логін/пароль: user/user</p>
                <?php if( !empty($_SESSION) && !empty($_SESSION['error_log'])):?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php echo $_SESSION['error_log'];?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>



