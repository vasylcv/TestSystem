<div class="container text-center">
    <div class="row pull-right">
        <h4><a href="archive" class="btn bg-primary">Архів</a></h4>
    </div>
    <div class="row"><h2>Виберіть тест для проходження</h2></div>
    <div class="row form-row">
        <form id="form-select" class="form-horizontal" action="site" method="POST">
            <select class="form-control" name="testId">
                <?php foreach ($data as $item):?>
                    <option value="<?php echo $item['id'];?>"><?php echo $item['title']. ' ('.$item['count'].')';?></option>
                <?php endforeach;?>
            </select>
            <button type="submit" class="btn btn-warning">Розпочати тестування</button>
        </form>
    </div>
</div>