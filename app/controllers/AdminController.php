<?php
require_once 'app/models/Test.php';
class AdminController extends Controller
{
    function execute()
    {
        if(!isset($_SESSION['role_id']) && !empty($_SESSION['role_id'])!= 1){
            parent::redirect('');
        }

        $test = new Test();

        if (!empty($_POST)){

            $testTitle = $_POST['testTitle'];
            $testDescription = $_POST['testDescription'];

            unset($_POST['testTitle'], $_POST['testDescription']);

            foreach($_POST as $k => $v) {
                foreach($v as $key => $val) {
                    $arrRes[$key][$k] = $val;
                }

            }

            $addTest = $test->set_test($testTitle, $testDescription, $arrRes);

            if($addTest == true){
                $_SESSION['status'] = 'Тест успішно доданий';
            }else{
                $_SESSION['error'] = 'Виникла помилка';
            }
        }

         $this->view->generate('admin_view.php','template_view.php');

        if(!empty($_SESSION['error'])){
            unset($_SESSION['error']);
        }

        if(!empty($_SESSION['status'])){
            unset($_SESSION['status']);
        }
    }
}