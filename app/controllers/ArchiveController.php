<?php
require_once 'app/models/QuestionUser.php';
class ArchiveController extends Controller
{
    function execute()
    {
        $questionUser = new QuestionUser();

        if(empty($_SESSION['user_id'])){
            parent::redirect('');
        }

         $user_id = $_SESSION['user_id'];

         $data = $questionUser->get_tests($user_id);

         if(empty($data)){
             parent::redirect('site');
             $_SESSION['error'] = 'У вас немає пройдених тестів';
         }

         $this->view->generate('archives_view.php','template_view.php', $data);
    }
}