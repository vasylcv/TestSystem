<?php
require_once 'app/models/Test.php';
require_once 'app/models/QuestionTest.php';
require_once 'app/models/Question.php';
require_once 'app/models/QuestionUser.php';
class SiteController extends Controller
{
    function execute()
    {
        $test = new Test();

        $questionTest = new QuestionTest();

        if(!empty($_POST['testId'])){

            $testID = $_POST['testId'];

            $qt_data = $questionTest->get_data($testID);

            $this->view->generate('test_view.php','template_view.php', $qt_data);

        }else{

            $t_data = $test->get_test();

            $this->view->generate('site_view.php','template_view.php', $t_data);
        }

        if(!empty($_SESSION['error'])){
            unset($_SESSION['error']);
        }

    }
}